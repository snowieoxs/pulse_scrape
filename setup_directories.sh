#!/bin/bash

# Create and set permissions for Prometheus directory
mkdir prometheus
sudo chown -R 65534:65534 prometheus
sudo chgrp -R $(whoami) prometheus
sudo chmod -R 775 prometheus

# Create and set permissions for Grafana directory
mkdir grafana
sudo chown -R 472:472 grafana
sudo chgrp -R $(whoami) grafana
sudo chmod -R 775 grafana
