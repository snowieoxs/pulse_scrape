#!/bin/bash

# Stop the Docker containers with a 30-second grace period
docker stop -t 180 grafana
docker stop -t 180 prometheus
docker stop -t 180 node_exporter
