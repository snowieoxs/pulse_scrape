#!/bin/bash

# Run node_exporter Docker container
docker run -dt --name node_exporter --restart=always \
--net='host' \
-v '/:/host:ro,rslave' \
prom/node-exporter --path.rootfs=/host 

# Run Prometheus Docker container
docker run -dt --name prometheus --restart=always \
--net='host' \
-v $(pwd)/prometheus.yml:/etc/prometheus/prometheus.yml \
-v $(pwd)/prometheus:/prometheus \
prom/prometheus

# Run Grafana Docker container
docker run -dt --name grafana --restart=always \
--net='host' \
-v $(pwd)/grafana:/var/lib/grafana \
grafana/grafana
