# Prometheus and Grafana monitor

This repository is for setting up Prometheus and Grafana to monitor geth and prysm validators, I wrote this thinking you would need to be in the cloned directory... but maybe you don't have to

## Geth flags

I had to add some flags to my Geth container to get this to work specifically

- ```--http```
- ```--metrics```
- ```--prof```

I believe docker automatically exposes ```8080,8081,8082``` for the beacon container

## Folder permissions so your containers can access your volumes

You can try running ```setup_directories.sh``` or you can copy these commands:

```bash
mkdir prometheus
sudo chown -R 65534:65534 prometheus
sudo chgrp -R $(whoami) prometheus
sudo chmod -R 775 prometheus
```

```bash
mkdir grafana
sudo chown -R 472:472 grafana
sudo chgrp -R $(whoami) grafana
sudo chmod -R 775 grafana
```

## Docker commands

Again you can try running ```run_containers.sh``` or you can copy paste the commands below:

```bash
docker run -dt --name node_exporter --restart=always \
--net='host' \
-v '/:/host:ro,rslave' \
prom/node-exporter --path.rootfs=/host 
```

```bash
docker run -dt --name prometheus --restart=always \
--net='host' \
-v $(pwd)/prometheus.yml:/etc/prometheus/prometheus.yml \
-v $(pwd)/prometheus:/prometheus \
prom/prometheus
```

```bash
docker run -dt --name grafana --restart=always \
--net='host' \
-v $(pwd)/grafana:/var/lib/grafana \
grafana/grafana
```

## Grafana Dashboard

Find the "+" on the top right of [localhost:3000](localhost:3000) and click Import dashboard.

Click "Upload dashboard JSON file" and navigate to ```dashboards``` and select the file ```dashboard.json```.

On the bottom of the "Import dashboard" there is a section called "prometheus" and it probably says "No data sources of type prometheus found" click the toggle and "Configure a new data source"

On the Add data source Screen, "Prometheus" should be at the top, click it, and make sure under the "Connection" section it is filled withj [http://localhost:9090](http://localhost:9090) the scroll down and click "Save & test"

Navigate back to the "Import dashboard" load the ```dashboard.json``` file and make sure your "prometheus" data source is selected. Then select "Import"

The dashboard should be working... I tried setting this up another way, and for some reason these were the steps I needed to follow to get it to work.

The "Trie stats" section of the dashboard is not working, and I'm not sure why, so maybe I'll look into that later.

I didn't test any of the other dashboards out, but did find them in [tsdlaines](https://github.com/tdslaine/install_pulse_node/blob/main/setup_monitoring.sh) pulse scripts, which I used a lot to figure this out... thanks tsdlaine!

I also used [David Feders](https://gitlab.com/davidfeder/validatorscript) pulsechain validator scripts to get my validator going, thanks a lot David!
